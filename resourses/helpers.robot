*** Settings ***
Library    RequestsLibrary
Library    FakerLibrary    locale=pt_br
Library    RPA.JSON
Library    OperatingSystem

*** Variables ***
${url}    https://serverest.dev

*** Keywords ***
Criar sessao
    [Arguments]    ${alias}
    Create Session    ${alias}    ${url}