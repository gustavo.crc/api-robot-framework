*** Settings ***
Resource    ../helpers.robot

*** Keywords ***
Realizar requisição para cadastrar usuario
    ${headers}     Create Dictionary    Content-Type=application/json
    # ${nome}        FakerLibrary.Name
    # ${email}       FakerLibrary.Email
    # ${senha}       FakerLibrary.Password    length=10    special_chars=False
    # ${data}        Convert String to JSON    {"nome": "${nome}", "email": "${email}", "password": "${senha}", "administrador": "true"}
    ${data}        Get File    data/cadastro_usuario.json
    ${data}        Evaluate    json.dumps(${data})    json
    ${response}    POST On Session    Usuarios
    ...                               ${url}/usuarios
    ...                               headers=${headers}
    ...                               data=${data}
    ...                               expected_status=201
    Set Suite Variable    ${response}
    Log To Console        Id do Usuario: ${response.json()}[_id]

Realizar requisição para tentativa de cadastro duplicado
    ${headers}     Create Dictionary    Content-Type=application/json
    ${data}        Convert String to JSON    {"nome": "Gustavo", "email": "alexandrefogaca@example.org", "password": "1234", "administrador": "true"}
    ${data}        Evaluate    json.dumps(${data})    json
    ${response}    POST On Session    Usuarios
    ...                               ${url}/usuarios
    ...                               headers=${headers}
    ...                               data=${data}
    ...                               expected_status=400
    Set Suite Variable    ${response}

Validar a seguinte mensagem de retorno do body: ${mensagem}
    Should Be Equal As Strings    ${response.json()}[message]    ${mensagem}
    