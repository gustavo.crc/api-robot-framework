*** Settings ***
Resource    ../helpers.robot

*** Keywords ***
Realizar requisição para deletar usuario
    ${headers}     Create Dictionary    Content-Type=application/json
    ${nome}        FakerLibrary.Name
    ${email}       FakerLibrary.Email
    ${senha}       FakerLibrary.Password    length=10    special_chars=False
    ${data}        Convert String to JSON    {"nome": "${nome}", "email": "${email}", "password": "${senha}", "administrador": "true"}
    ${data}        Evaluate    json.dumps(${data})    json
    ${response_post}    POST On Session    Usuarios    ${url}/usuarios    headers=${headers}    data=${data}    expected_status=201
    Set Suite Variable    ${response_post}
    Log To Console        Id do Usuario: ${response_post.json()}[_id]

    ${response_delete}    DELETE On Session    Usuarios    ${url}/usuarios/${response_post.json()}[_id]    headers=${headers}    expected_status=200
    Set Suite Variable    ${response_delete}

Validar mensagem de retorno no body: "${mensagem}"
    Should Be Equal As Strings    ${response_delete.json()}[message]    ${mensagem}