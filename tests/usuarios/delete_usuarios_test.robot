*** Settings ***
Resource    ../../resourses/helpers.robot
Resource    ../../resourses/usuarios/delete_usuarios.robot

*** Test Cases ***
Validar que um usuario seja deletado com sucesso
    [Tags]          Regressao    ExclusaoUsuario
    Criar sessao    Usuarios
    Realizar requisição para deletar usuario
    Validar mensagem de retorno no body: "Registro excluído com sucesso"