*** Settings ***
Resource    ../../resourses/helpers.robot
Resource    ../../resourses/usuarios/post_usuarios.robot

*** Test Cases ***
Validar cadastro de um novo usuario
    [Tags]          Regressao    CadastroUsuario
    Criar sessao    Usuarios
    Realizar requisição para cadastrar usuario
    Validar a seguinte mensagem de retorno do body: Cadastro realizado com sucesso
    
Validar tentativa de cadastro de usuario duplicado
    [Tags]          Regressao    CadastroUsuarioDuplicado
    Criar sessao    Usuarios
    Realizar requisição para tentativa de cadastro duplicado
    Validar a seguinte mensagem de retorno do body: Este email já está sendo usado